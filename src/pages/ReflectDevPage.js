import React, { PureComponent } from 'react';
import CupcakeIpsum from '../components/CupcakeIpsum';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

export default class ReflectDevPage extends PureComponent {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>ReflectDev - Collaborative Professional Development Framework</PageTitle>
        <CupcakeIpsum paragraphs={5} />
      </ContentWrapper>
    );
  }
}
