import React, { PureComponent } from 'react';
import CupcakeIpsum from '../components/CupcakeIpsum';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

export default class ProductDevPage extends PureComponent {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>ProductDev - Collaborative Product Development Framework</PageTitle>
        <CupcakeIpsum paragraphs={5} />
      </ContentWrapper>
    );
  }
}
